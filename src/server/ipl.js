// 1. Number of matches played per year for all the years in IPL.
function matchesPerYear(matches) {
    let seasonMatchid = {}
    for (let row of matches) {
        if (seasonMatchid.hasOwnProperty(row['season'])) {
            seasonMatchid[row['season']].push(row['id'])
        } else {
            seasonMatchid[row['season']] = [row['id']]
        }
    }

    let totalMatchPerYear = {}

    for (let [season, id] of Object.entries(seasonMatchid)) {
        totalMatchPerYear[season] = id.length
    }
    return totalMatchPerYear
}


// 2. Number of matches won per team per year in IPL.
function matchWonPerTeam(matches) {
    let Match_won_per_team = {}
    for (let row of matches) {
        if (Match_won_per_team.hasOwnProperty(row['season'])) {
            Match_won_per_team[row['season']] = {}
        } else {
            Match_won_per_team[row['season']] = {}
        }
    }

    for (let row of matches) {
        if (row['season'] in Match_won_per_team) {
            if (row['winner'] in Match_won_per_team[row['season']]) {
                Match_won_per_team[row['season']][row['winner']] += 1
            } else {
                Match_won_per_team[row['season']][row['winner']] = 1
            }
        }
    }
    return Match_won_per_team
}


// 3. Extra runs conceded per team in the year 2016
function ExtraRuns(matches, deliveries) {
    let seasionId2016 = {}
    for (let row of matches) {
        if (row['season'] == 2016) {
            if (seasionId2016.hasOwnProperty(row['season'])) {
                seasionId2016[row['season']].push(row['id'])
            } else {
                seasionId2016[row['season']] = [row['id']]
            }
        }
    }

    let MatchId2016 = []

    for (let [season, ids] of Object.entries(seasionId2016)) {
        for (let id of ids) {
            MatchId2016.push(id)
        }
    }

    let ExtraRunsPerTeam = {}

    for (let row of deliveries) {
        if (MatchId2016.includes(row['match_id'])) {
            if (ExtraRunsPerTeam.hasOwnProperty(row['bowling_team'])) {
                ExtraRunsPerTeam[row['bowling_team']] += parseInt(row['extra_runs'])
            } else {
                ExtraRunsPerTeam[row['bowling_team']] = parseInt(row['extra_runs'])
            }
        }
    }
    let ExtraRunsPerTeam2016 = {}

    for (let [season, id] of Object.entries(seasionId2016)) {
        ExtraRunsPerTeam2016[season] = ExtraRunsPerTeam
    }
    return ExtraRunsPerTeam2016
}

console.log(ExtraRuns(matches, deliveries))

// 4. Top 10 economical bowlers in the year 2015

function Economical(matches, deliveries) {
    let seasonId = {}
    for (let row of matches) {
        if (row['season'] == 2015) {
            if (seasonId.hasOwnProperty(row['season'])) {
                seasonId[row['season']].push(row['id'])
            } else {
                seasonId[row['season']] = [row['id']]
            }
        }
    }

    let match_id = []

    for (let [season, ids] of Object.entries(seasonId)) {
        for (let id of ids) {
            match_id.push(id)
        }
    }

    let bowlerBowled = {}

    for (let row of deliveries) {
        if (match_id.includes(row['match_id'])) {
            if (bowlerBowled.hasOwnProperty(row['bowler'])) {
                bowlerBowled[row['bowler']] += 1
            } else {
                bowlerBowled[row['bowler']] = 1

            }
        }
    }

    let RunsGivenBybowler = {}

    for (let row of deliveries) {
        if (match_id.includes(row['match_id'])) {
            if (RunsGivenBybowler.hasOwnProperty(row['bowler'])) {
                RunsGivenBybowler[row['bowler']] += parseInt(row['total_runs'])
            } else {
                RunsGivenBybowler[row['bowler']] = parseInt(row['total_runs'])
            }
        }
    }

    let EconomicalBowler = {}

    for (let [bowler, bowler_bowled] of Object.entries(bowlerBowled)) {
        EconomicalBowler[bowler] = parseFloat(((RunsGivenBybowler[bowler] / bowler_bowled) * 6).toFixed(4))
    }
    const sortedEconomicalBowler = Object.entries(EconomicalBowler).sort((a, b) => b[1] - a[1]).reverse().slice(0, 10)

    let Top_10_Economical_bowler = {}
    for (let EconomicalBowler of sortedEconomicalBowler) {
        Top_10_Economical_bowler[EconomicalBowler[0]] = { 'economyRate': EconomicalBowler[1] }
    }
    return Top_10_Economical_bowler
}
module.exports = {
    matchesPerYear,
    matchWonPerTeam,
    ExtraRuns,
    Economical
}
