const csv = require('csvtojson');
const fs = require('fs');

const matchesCsv = '../data/matches.csv';
const deliveriesCsv = '../data/deliveries.csv';

const matchesJson = '../data/matches.json';
const deliveriesJson = '../data/deliveries.json'


function csvToJson(filePath,fileName){
    csv()
    .fromFile(filePath)
    .then((json)=>{  
    fs.writeFile(fileName, JSON.stringify(json), (err) => { 
        if (err) console.log(err)
    }) 
}) 
}

csvToJson(matchesCsv,matchesJson);
csvToJson(deliveriesCsv,deliveriesJson);


//dumping into JSON file

const matches = require('../data/matches.json')
const deliveries = require('../data/deliveries.json')

const iplFile = require('./ipl.js')

function writefile(filePath,fileName){
    fs.writeFile(filePath,JSON.stringify(fileName) , err => {
        if (err) {
            console.log('error')
        } else {
            console.log('success')
        }
    })
}


const matches_per_year = iplFile.matchesPerYear(matches)
const filePath_1 = '../public/output/matchesPerYear.json'
writefile(filePath_1,matches_per_year)

const match_won_per_team = iplFile.matchWonPerTeam(matches)
const filePath_2 = '../public/output/matchWonPerTeam.json'
writefile(filePath_2,match_won_per_team)

const extra_runs = iplFile.ExtraRuns(matches,deliveries)
const filePath_3 = '../public/output/extraRuns.json'
writefile(filePath_3,extra_runs)

const economical =iplFile.Economical(matches,deliveries)
const filePath_4 = '../public/output/top10EconomicalBowler.json'
writefile(filePath_4,economical)

